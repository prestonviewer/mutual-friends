var authorizeUrl = 'https://oauth.vk.com/authorize?client_id=3998728&scope=friends&redirect_uri=http://rubanyuk.ru/mutual&display=mobile&response_type=token';
var STORAGE_KEY = 'VK_AUTH_DATA';

function authorize() {
	location.href = authorizeUrl;
}

function api(method, args, callback) {
	var base = "https://api.vk.com/method/", version = "v=5.27",opts = [], request = "";
	
	var keys = Object.keys(args);
	if (keys.length !== 0) {
		opts = keys.map(function(key) {
			var value = args[key];
			if (value instanceof Array) {
				value = value.join(',');
			}

			return [key, value].join('=');
		});
	}

	opts.push(version);
	request = base + method + '?' + opts.join('&');
	
	return $.ajax({
		url: request,
		dataType: "jsonp",
		type: 'GET',
		success: callback
	});
}

function applyAuthorization() {
	var params = parseHashParams();
	
	if (params.access_token) {
		params.expires_in = Date.now() + params.expires_in * 1000;
		localStorage.setItem(STORAGE_KEY, {
			access_token: params.access_token,
			expires_in: params.expires_in
		});
		
		location.hash = '';
	}
}

function isLoggedIn() {
	var data = JSON.parse(localStorage.getItem(STORAGE_KEY) || '{}');
	if (data && data.access_token && data.expires_in) {
		return Date.now() < data.expires_in || data.expires_in === 0;
	}
	
	return false;
}

function parseHashParams() {
	return location.hash.substr(1)
		.split('&')
		.map(function(part) {
			return part.split('=');
		})
		.reduce(function(acc, part) {
			acc[part[0]] = part.length === 2 ? part[1] : true;
			return acc;
		}, {});
}

function start() {
	applyAuthorization();
	
	if (isLoggedIn()) {
		boostrap();
	} else {
		authorize();
	}
}

function boostrap() {
	var users = new (Backbone.Collection.extend({
		model: Backbone.Model.extend({
			fetch: function() {
				
			}
		})
	}))();
}

// start();